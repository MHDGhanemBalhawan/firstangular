import { Injectable } from '@angular/core';
import { Promotion } from '/home/ghanem/code/coursera/angular/conFusion/src/app/menu/shared/promotion';
import { PROMOTIONS } from '/home/ghanem/code/coursera/angular/conFusion/src/app/menu/shared/promotions';

@Injectable()
export class PromotionService {

  constructor() { }

  getPromotions(): Promotion[] {
    return PROMOTIONS;
  }
  getPromotion(id: number): Promotion {
    return PROMOTIONS.filter((promo) => (promo.id === id))[0];
  }

  getFeaturedPromotion(): Promotion {
    return PROMOTIONS.filter((promotion) => promotion.featured)[0];
  }
}
