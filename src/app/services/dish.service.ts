import { Injectable } from '@angular/core';
import { Dish } from '/home/ghanem/code/coursera/angular/conFusion/src/app/menu/shared/dish';
import { DISHES } from '/home/ghanem/code/coursera/angular/conFusion/src/app/menu/shared/dishes';


@Injectable()
export class DishService {

  constructor() { }

  getDishes(): Dish[] {
    return DISHES;
  }
    getDish(id: number): Dish {
    return DISHES.filter((dish) => (dish.id === id))[0];
  }

  getFeaturedDish(): Dish {
    return DISHES.filter((dish) => dish.featured)[0];
  }
}
